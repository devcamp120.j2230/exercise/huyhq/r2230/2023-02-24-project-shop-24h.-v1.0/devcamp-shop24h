import { Row, Card, CardBody, CardTitle, CardText, CardSubtitle, Button, Col } from "reactstrap";
const textDecoration = {
    textDecoration: "line-through",
    fontSize: "12px",
    color: "gray"
}
const LastestProductsComponent = () => {
    return (
        <Row className="text-center justify-content-center mt-5">
            <h1 className="my-3">Latest Product</h1>
            <Col lg="3" md="6" sm="12">
                <Card style={{ width: '18rem' }} className="border-0">
                    <img alt="Sample" src="https://cdn2.cellphones.com.vn/358x358,webp,q100/media/catalog/product/t/_/t_m_18.png" />
                    <CardBody>
                        <CardTitle tag="h5">
                            Samsung Galaxy S22 Ultra
                        </CardTitle>
                        <CardText>
                            <span className="me-2" style={textDecoration}>600</span>
                            <span className="text-danger"><b>500</b></span>
                        </CardText>
                    </CardBody>
                </Card>
            </Col>
            <Col lg="3" md="6" sm="12">
                <Card style={{ width: '18rem' }} className="border-0">
                    <img alt="Sample" src="https://cdn2.cellphones.com.vn/358x358,webp,q100/media/catalog/product/t/_/t_m_18.png" />
                    <CardBody>
                        <CardTitle tag="h5">
                            Samsung Galaxy S22 Ultra
                        </CardTitle>
                        <CardText>
                            <span className="me-2" style={textDecoration}>600</span>
                            <span className="text-danger"><b>500</b></span>
                        </CardText>
                    </CardBody>
                </Card>
            </Col>
            <Col lg="3" md="6" sm="12">
                <Card style={{ width: '18rem' }} className="border-0">
                    <img alt="Sample" src="https://cdn2.cellphones.com.vn/358x358,webp,q100/media/catalog/product/t/_/t_m_18.png" />
                    <CardBody>
                        <CardTitle tag="h5">
                            Samsung Galaxy S22 Ultra
                        </CardTitle>
                        <CardText>
                            <span className="me-2" style={textDecoration}>600</span>
                            <span className="text-danger"><b>500</b></span>
                        </CardText>
                    </CardBody>
                </Card>
            </Col>
            <Col lg="3" md="6" sm="12">
                <Card style={{ width: '18rem' }} className="border-0">
                    <img alt="Sample" src="https://cdn2.cellphones.com.vn/358x358,webp,q100/media/catalog/product/t/_/t_m_18.png" />
                    <CardBody>
                        <CardTitle tag="h5">
                            Samsung Galaxy S22 Ultra
                        </CardTitle>
                        <CardText>
                            <span className="me-2" style={textDecoration}>600</span>
                            <span className="text-danger"><b>500</b></span>
                        </CardText>
                    </CardBody>
                </Card>
            </Col>
            <Col lg="3" md="6" sm="12">
                <Card style={{ width: '18rem' }} className="border-0">
                    <img alt="Sample" src="https://cdn2.cellphones.com.vn/358x358,webp,q100/media/catalog/product/t/_/t_m_18.png" />
                    <CardBody>
                        <CardTitle tag="h5">
                            Samsung Galaxy S22 Ultra
                        </CardTitle>
                        <CardText>
                            <span className="me-2" style={textDecoration}>600</span>
                            <span className="text-danger"><b>500</b></span>
                        </CardText>
                    </CardBody>
                </Card>
            </Col>
            <Col lg="3" md="6" sm="12">
                <Card style={{ width: '18rem' }} className="border-0">
                    <img alt="Sample" src="https://cdn2.cellphones.com.vn/358x358,webp,q100/media/catalog/product/t/_/t_m_18.png" />
                    <CardBody>
                        <CardTitle tag="h5">
                            Samsung Galaxy S22 Ultra
                        </CardTitle>
                        <CardText>
                            <span className="me-2" style={textDecoration}>600</span>
                            <span className="text-danger"><b>500</b></span>
                        </CardText>
                    </CardBody>
                </Card>
            </Col>
            <Col lg="3" md="6" sm="12">
                <Card style={{ width: '18rem' }} className="border-0">
                    <img alt="Sample" src="https://cdn2.cellphones.com.vn/358x358,webp,q100/media/catalog/product/t/_/t_m_18.png" />
                    <CardBody>
                        <CardTitle tag="h5">
                            Samsung Galaxy S22 Ultra
                        </CardTitle>
                        <CardText>
                            <span className="me-2" style={textDecoration}>600</span>
                            <span className="text-danger"><b>500</b></span>
                        </CardText>
                    </CardBody>
                </Card>
            </Col>
            <Col lg="3" md="6" sm="12">
                <Card style={{ width: '18rem' }} className="border-0">
                    <img alt="Sample" src="https://cdn2.cellphones.com.vn/358x358,webp,q100/media/catalog/product/t/_/t_m_18.png" />
                    <CardBody>
                        <CardTitle tag="h5">
                            Samsung Galaxy S22 Ultra
                        </CardTitle>
                        <CardText>
                            <span className="me-2" style={textDecoration}>600</span>
                            <span className="text-danger"><b>500</b></span>
                        </CardText>
                    </CardBody>
                </Card>
            </Col>

        </Row>
    )
}

export default LastestProductsComponent;