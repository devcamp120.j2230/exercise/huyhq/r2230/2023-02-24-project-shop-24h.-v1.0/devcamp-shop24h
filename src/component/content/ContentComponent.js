
import { Col } from "reactstrap";
import CarouselComponent from "./CarouselComponent";
import LastestProductsComponent from "./LastestProductsComponent";
import ViewAllComponent from "./ViewAllComponent";


const ContentComponent = () => {
    return (
        <Col className="my-5">
            <CarouselComponent/>
            <LastestProductsComponent/>
            <ViewAllComponent/>
        </Col>
    );
}

export default ContentComponent;