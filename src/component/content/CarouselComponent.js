import Slider from "react-slick";
import { Button, Col, Row } from "reactstrap";

const CarouselComponent = () => {
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1
    };
    return (
        <Col>
            <Slider {...settings}>
                <Row className="d-flex">
                    <Col>
                        <Row>
                            <p className="font-weight-bold">New Product</p>
                        </Row>
                        <Row>
                            <h1>iPhone 14 Pro Max 128GB</h1>
                        </Row>
                        <Row>
                            <p>iPhone 14 Pro Max là mẫu flagship nổi bật nhất của Apple trong lần trở lại năm 2022 với nhiều cải tiến về công nghệ cũng như vẻ ngoài cao cấp, sang chảnh hợp với gu thẩm mỹ đại chúng. Những chiếc điện thoại đến từ nhà Táo Khuyết nhận được rất nhiều sự kỳ vọng của thị trường ngay từ khi chưa ra mắt. Vậy liệu những chiếc flagship đến từ công ty công nghệ hàng đầu thế giới này có làm bạn thất vọng? Cùng khám phá những điều thú vị về iPhone 14 Pro Max ở bài viết dưới đây nhé.</p>
                        </Row>
                        <Col>
                            <Button color="primary">Shop now</Button>
                        </Col>
                    </Col>
                    <Col xs="6" className="d-flex justify-content-center">
                        <img src="https://cdn2.cellphones.com.vn/358x358,webp,q100/media/catalog/product/t/_/t_m_18.png"/>
                    </Col>
                </Row>
                <Row className="d-flex">
                    <Col>
                        <Row>
                            <p className="font-weight-bold">New Product</p>
                        </Row>
                        <Row>
                            <h1>Samsung Galaxy S22 Ultra</h1>
                        </Row>
                        <Row>
                            <p>Đúng như các thông tin được đồn đoán trước đó, mẫu flagship mới của gả khổng lồ Hàn Quốc được ra mắt với tên gọi là Samsung Galaxy S22 Ultra với nhiều cải tiến đáng giá. Mẫu điện thoại cao cấp đến từ Samsung này có nhiều thay đổi từ thiết kế, cấu hình cho đến camera. Vậy siêu phẩm này có gì mới, giá bao nhiêu và có nên mua không? Hãy cùng tìm hiểu chi tiết ngay bên dưới nhé!</p>
                        </Row>
                        <Col>
                            <Button color="primary">Shop now</Button>
                        </Col>
                    </Col>
                    <Col xs="6" className="d-flex justify-content-center">
                        <img src="https://cdn2.cellphones.com.vn/358x358,webp,q100/media/catalog/product/s/m/sm-s908_galaxys22ultra_front_burgundy_211119.jpg"/>
                    </Col>
                </Row>
            </Slider>
        </Col>
    )
};

export default CarouselComponent;