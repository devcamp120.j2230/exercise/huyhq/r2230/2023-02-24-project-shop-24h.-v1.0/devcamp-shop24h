import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBell, faCartShopping, faUser } from "@fortawesome/free-solid-svg-icons";

import { Col, Row } from "reactstrap";

const HeaderIconNavBar = () => {
    return (
        <Col xs="4">
            <Row>
                <Col className="text-center">
                    <a href="#" className="header-icon me-3"><FontAwesomeIcon icon={faBell} /></a>
                    <a href="#" className="header-icon me-3"><FontAwesomeIcon icon={faUser} /></a>
                    <a href="#" className="header-icon me-3"><FontAwesomeIcon icon={faCartShopping} /></a>
                </Col>
            </Row>
        </Col>
    )
};

export default HeaderIconNavBar;