import { Col, Row } from "reactstrap";

const HeaderLogo = () => {
    return (
        <Col className="text-start">
            <h1>DevCamp</h1>
        </Col>
    );
};

export default HeaderLogo;