import { Col, Row } from "reactstrap";
import HeaderIconNavBar from "./HeaderIconNavBar";
import HeaderLogo from "./HeaderLogo";
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css"></link>
const HeaderComponent = () => {
    return (
        <Row className="p-5">
            <HeaderLogo />
            <HeaderIconNavBar />
        </Row>
    );
}

export default HeaderComponent;