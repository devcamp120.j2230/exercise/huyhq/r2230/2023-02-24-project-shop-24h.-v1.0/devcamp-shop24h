import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";

import ContentComponent from './component/content/ContentComponent';
import FooterComponent from './component/footer/FooterComponent';
import HeaderComponent from './component/header/HeaderComponent';

function App() {
  return (
    <div className="container">
      <HeaderComponent/>
      <ContentComponent/>
      <FooterComponent/>
    </div>
  );
}

export default App;
